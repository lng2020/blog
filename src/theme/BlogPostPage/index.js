// Ejected unsafe, need to check if this changes and maintain this component
// https://github.com/facebook/docusaurus/blob/main/packages/docusaurus-theme-classic/src/theme/BlogPostPage/index.tsx
import React from "react";
import clsx from "clsx";
import {HtmlClassNameProvider, ThemeClassNames} from "@docusaurus/theme-common";
import {BlogPostProvider, useBlogPost} from "@docusaurus/theme-common/internal";
import BlogLayout from "@theme/BlogLayout";
import BlogPostItem from "@theme/BlogPostItem";
import BlogPostPaginator from "@theme/BlogPostPaginator";
import BlogPostPageMetadata from "@theme/BlogPostPage/Metadata";
import BlogPostCoverImage from "@theme/BlogReleaseCoverImage";
import TOC from "@theme/TOC";
import styles from "./styles.module.css";

// customized:
// - Hide toc
// - Added Cover Image to the post
function BlogPostPageContent({sidebar, children}) {
  const {metadata, toc} = useBlogPost();
  const {nextItem, prevItem, frontMatter, title} = metadata;
  const {
    hide_table_of_contents: hideTableOfContents,
    toc_min_heading_level: tocMinHeadingLevel,
    toc_max_heading_level: tocMaxHeadingLevel,
  } = frontMatter;
  return (
    <BlogLayout
      sidebar={sidebar}
      toc={undefined}
    >
      <BlogPostItem>
        {frontMatter.coverImageRelease ? (<figure>
          {BlogPostCoverImage({ version: frontMatter.coverImageRelease })}
        </figure>)
        : frontMatter.coverImage && <figure>
          <img
            alt={`Banner for blog post with title "${title}"`}
            className={clsx(styles.image, {
              [styles["image--title"]]: children != null,
            })}
            src={frontMatter.coverImage}
            loading="lazy"
          />
        </figure>}
        {children}
      </BlogPostItem>

      {(nextItem || prevItem) && (
        <BlogPostPaginator nextItem={nextItem} prevItem={prevItem} />
      )}
    </BlogLayout>
  );
}
export default function BlogPostPage(props) {
  const BlogPostContent = props.content;
  return (
    <BlogPostProvider content={props.content} isBlogPostPage>
      <HtmlClassNameProvider
        className={clsx(
          ThemeClassNames.wrapper.blogPages,
          ThemeClassNames.page.blogPostPage,
        )}>
        <BlogPostPageMetadata />
        <BlogPostPageContent sidebar={props.sidebar}>
          <BlogPostContent />
        </BlogPostPageContent>
      </HtmlClassNameProvider>
    </BlogPostProvider>
  );
}
